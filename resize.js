import sharp from 'sharp'
import fs from 'fs'
import path from 'path'

var imagesDir = './images';

fs.readdir(imagesDir, (err, files) => {
  if (err) {
    console.error("Error reading directory:", err);
    return;
  }

  console.log(`Found ${files.length} files in directory ${imagesDir}`);
  
  files.forEach(file => {
    if(path.extname(file) === '.webp') {
      console.log(`Processing file: ${file}`);
      
      sharp(`${imagesDir}/${file}`)
        .metadata()
        .then(metadata => {
          // Check the dimensions of the image
          if (metadata.width > 1000 || metadata.height > 1000) {
            console.log(`Resizing file (downscale): ${file}`);
            return sharp(`${imagesDir}/${file}`)
              .resize(1000, 1000, {
                fit: 'inside'
              })
              .webp()
              .toFile(`${imagesDir}/../${path.basename(file, '.webp')}.webp`);
          } else {
            console.log(`Resizing file (upscale): ${file}`);
            return sharp(`${imagesDir}/${file}`)
              .resize(1000, 1000, {
                fit: 'outside'
              })
              .webp()
              .toFile(`${imagesDir}/../${path.basename(file, '.webp')}.webp`);
          }
        })
        .then(function(info) {
          console.log(`File processed successfully: ${file}`, info);
        })
        .catch(function(err) {
          console.error(`Error processing file: ${file}`, err);
        });
    }
  })
});
