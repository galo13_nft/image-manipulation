import sharp from 'sharp'
import fs from 'fs'
import path from 'path'

var imagesDir = './images';

fs.readdir(imagesDir, (err, files) => {
  if (err)
    console.log(err);
  else {  
    console.log(`Found ${files.length} files in directory ${imagesDir}`);
    files.forEach(file => {
      if(path.extname(file) === '.jpg') {
        console.log(file);
        sharp(`${imagesDir}/${file}`)
          .webp()
          .toFile(`${imagesDir}/${path.basename(file, '.jpg')}.webp`)
          .then(function(info) {
              console.log(info)
          })
          .catch(function(err) {
              console.log(err)
          });
      }
    })
  }
});